<?

include_once '_mysqli.php';

$Q = 'SELECT 	`language`
        FROM   	`jc_language`
        WHERE  	`delete` IS NULL 
        AND 	`key_user` = "' . $params['user'] . '"';

$RS = $mysqli->query( $Q );

if( $RS->num_rows == 0 )
{

	$lang = array(

		'iface' => 'vi',
		'card' => array(
			'vi' => true,
			'en' => false,
			'ko' => false
		)
	);

	$Q = '	INSERT INTO `jc_language`( 	`key_user`, 
  										`language`, 
  										`insert` 	) 
			VALUES ( 					"' . $_GET['user'] . '", 
										"' . addslashes( json_encode( $lang ) ) . '", 
										NOW() 		);';
	$mysqli->query( $Q );

}
else
{

	$R = $RS->fetch_array( );

	$lang = json_decode( ($R['language']), true );

}

$QLang = ' AND `language` in ( ';
if( $lang['card']['vi'] == 1 )
{
	$QLang .= '"vi",';
}
if( $lang['card']['en'] == 1 )
{
	$QLang .= '"en",';
}
if( $lang['card']['ko'] == 1 )
{
	$QLang .= '"ko",';
}
$QLang = substr( $QLang, 0, strlen( $QLang ) - 1 );
$QLang .= ') ';

$Q = 'SELECT *,

      ( SELECT `key`
        FROM   `jc_like`
        WHERE  `delete`   IS NULL
        AND    `key_user` = "' . $params['user'] . '"
        AND    `type`     = "card"
        AND    `vingle`   = `jc_card`.`vingle`
        LIMIT  1 )
        AS     `my_like`,

      ( SELECT `key`
        FROM   `jc_clip`
        WHERE  `delete`   IS NULL
        AND    `key_user` = "' . $params['user'] . '"
        AND    `card`     = `jc_card`.`vingle`
        LIMIT  1 )
        AS     `my_clip`

        FROM   `jc_card`
        WHERE  `delete` IS NULL ';

$Q .= 'AND `vingle` NOT IN (SELECT `vingle` FROM `jc_censor` WHERE `delete` IS NULL AND `type` = "card" AND `key_user` = "' . $params['user'] . '" ) ';
$Q .= $QLang;
if( $params['type'] == 'pop' )
	$Q .= ' ORDER BY `vingle` DESC ';
if( $params['type'] == 'like' )
	$Q .= ' ORDER BY `like` DESC ';
if( $params['type'] == 'comment' )
	$Q .= ' ORDER BY `comment` DESC ';
if( $params['type'] == 'clip' )
	$Q .= ' AND `vingle` IN ( SELECT `card` FROM `jc_clip` WHERE `delete` IS NULL AND `key_user` = "' . $params['user'] . '") ';
if( $params['type'] == 'interest' )
{
	$QI = 'SELECT `interest`
        FROM   	`jc_interest`
        WHERE  	`delete` IS NULL
        AND		`key_user` = "' . $params['user'] . '"';

	$RSI = $mysqli->query( $QI );
	if( $RS->num_rows != 0 )
	{
		$i = 0;
		while( $R = $RSI->fetch_assoc( ) )
		{
			if( $i++ == 0 )
			{
				$Q .= " AND ( `interest` like '%" . $R['interest'] . "%'";
			}
			else
			{

			}
			$Q .= " OR `interest` like '%" . $R['interest'] . "%'";
		}
		if( $i != 0 )
		{
			$Q .= ' ) ';
		}
	}
	$Q .= ' ORDER BY `vingle` DESC ';
}

$Q .= 'LIMIT  10
         OFFSET ' . (10 * $params['page']);
$RS = $mysqli->query( $Q );
$result = array( );
while( $R = $RS->fetch_assoc( ) )
	$result[] = $R;

mysqli_close( $mysqli );

return $result;
?>
