<?

include_once '_mysqli.php';

// Duplication Check

$Q = 'SELECT `key`
        FROM   `jc_interest`
        WHERE  `delete`   IS NULL
        AND    `key_user` = "' . $params['user'] . '"
        AND    `interest`   = "' . $params['interest'] . '"';

$RR = $mysqli->query( $Q );
$url = 'https://www.vingle.net/api/parties/' . $params['interest'];
$IN = json_decode( file_get_contents( $url ) );

// Get token user
$Q = '	SELECT `mail`, `collection`, `token` 
			FROM `jc_user`
			WHERE `key` = "' . $params['user'] . '"';
$RS = $mysqli->query( $Q );
$U = $RS->fetch_array( );
if( $U['token'] == 0 || $U['collection'] == 0 )
{
	$url = 'https://api1.vingle.net/api/censor/user?email=' . $U['mail'];
	$attribute = array( 'http' => array(
			'method' => "GET",
			'header' => "X-Vingle-Application-Id: 4f9e15f86042c57681cc13e46cda6f92\r\n" . "X-Vingle-Rest-Api-Token: bbc5516b902f10ffe6404d1ff60443d9\r\n"
		) );
	$context = stream_context_create( $attribute );
	$U = json_decode( file_get_contents( $url, false, $context ) );
	$T = $U->token;
	$CO = $U->collections[0]->id;
	$mysqli->query( "	UPDATE `jc_user` 
							SET `token` = '" . $T . "', `collection` = '" . $CO . "'
							WHERE `key` = '" . $params['user'] . "'" );
}
else
{
	$T = $U['token'];
}

if( $RR->num_rows == 0 )
{

	// Insert

	$Q = 'INSERT INTO `jc_interest` ( `key_user`     ,
                                  `interest`       ,
                                  `insert`        )
          VALUES                ( "' . $params['user'] . '",
                                  "' . $params['interest'] . '",
                                  NOW( )                    )';

	$mysqli->query( $Q );

	$result = $mysqli->insert_id;

	$url = 'https://api1.vingle.net/api/parties/' . $IN->id . '/fandoms';
	$cURL = curl_init( );
	curl_setopt_array( $cURL, array(
		CURLOPT_URL => $url,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_HTTPHEADER => array(
			"X-Vingle-Authentication-Token: " . $T,
			"X-Vingle-Application-Id: 4f9e15f86042c57681cc13e46cda6f92",
			"X-Vingle-Rest-Api-Token: bbc5516b902f10ffe6404d1ff60443d9",
			"Content-Length:          0                               ",
			"Content-Type:            application/json                ",
			"Accept:                  application/json                "
		),
		CURLOPT_RETURNTRANSFER => 1
	) );
	curl_exec( $cURL );

}
else
{
	$Q = '	UPDATE `jc_interest` 
  			SET  `delete` = NOW()
			WHERE	`interest` =  "' . $params['interest'] . '" AND 
					`key_user` = "' . $params['user'] . '"';

	$mysqli->query( $Q );

	$result = $mysqli->affected_rows;

	$url = 'https://api1.vingle.net/api/parties/' . $IN->id . '/fandoms';
	$cURL = curl_init( );
	curl_setopt_array( $cURL, array(
		CURLOPT_URL => $url,
		CURLOPT_CUSTOMREQUEST => "DELETE",
		CURLOPT_HTTPHEADER => array(
			"X-Vingle-Authentication-Token: " . $T,
			"X-Vingle-Application-Id: 4f9e15f86042c57681cc13e46cda6f92",
			"X-Vingle-Rest-Api-Token: bbc5516b902f10ffe6404d1ff60443d9",
			"Content-Length:          0                               ",
			"Content-Type:            application/json                ",
			"Accept:                  application/json                "
		),
		CURLOPT_RETURNTRANSFER => 1
	) );
	curl_exec( $cURL );
}

mysqli_close( $mysqli );

return $result;
?>
