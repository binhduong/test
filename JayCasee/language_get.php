<?

  include_once '_mysqli.php';

  $Q = 'SELECT 	`language`
        FROM   	`jc_language`
        WHERE  	`delete` IS NULL 
        AND 	`key_user` = "' . $params['user'] . '"';

  $RS = $mysqli -> query( $Q );
  
  if( $RS -> num_rows == 0 ) {
  	
	$result = array(
	
		'iface' => 'vi',
		'card' 		=> array( 
					'vi' => true, 
					'en' => false, 
					'ko' => false ));
  	
  	$Q = '	INSERT INTO `jc_language`( 	`key_user`, 
  										`language`, 
  										`insert` 	) 
			VALUES ( 					"' . $params['user'] . '", 
										"' . addslashes(json_encode($result)) . '", 
										NOW() 		);';
	$mysqli -> query( $Q );
	
  } else {
  	
  	$R = $RS -> fetch_array();
	
	$result = json_decode(($R['language']), true);
	
  }

  mysqli_close( $mysqli );

  return $result;

?>
