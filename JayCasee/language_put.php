<?

  include_once '_mysqli.php';

  $Q = 'SELECT 	`language`
        FROM   	`jc_language`
        WHERE  	`delete` IS NULL 
        AND 	`key_user` = "' . $params['user'] . '"';

  $RS = $mysqli -> query( $Q );
  $C = explode(',', $params['card']);
  $vi = false;
  $en = false;
  $ko = false;
  for($i = 0; $i < count($C); $i++)
  {
  	if($C[$i] == 'vi')
	{
		$vi = true;
	}
	if($C[$i] == 'en')
	{
		$en = true;
	}
	if($C[$i] == 'ko')
	{
		$ko = true;
	}	
  }
  
  $R = array(
	
		'iface' => $params['interface'],
		'card' 		=> array( 
					'vi' => $vi, 
					'en' => $en, 
					'ko' => $ko ));
  
  if( $RS -> num_rows == 0 ) {
  	
	
  	
  	$Q = '	INSERT INTO `jc_language`( 	`key_user`, 
  										`language`, 
  										`insert` 	) 
			VALUES ( 					"' . $params['user'] . '", 
										"' . addslashes(json_encode($R)) . '", 
										NOW() 		);';
	$mysqli -> query( $Q );
	$result = $mysqli -> insert_id;
	
  } else {
  	
	
	$Q = 'UPDATE `jc_language`
          SET    `language`   = "' . addslashes(json_encode($R)) . '", `update` = NOW()
          WHERE  `key_user` = "' . $params['user'] . '"';

    $mysqli -> query( $Q );
	
	$result = $mysqli -> affected_rows;
  }

  mysqli_close( $mysqli );

  return $result;

?>
