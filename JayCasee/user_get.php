<?

  include_once '_mysqli.php';



  // GUID & UUID

  $R = $mysqli -> query( 'SELECT   `key`,
                                   `key_user`
                          FROM     `jc_user_device`
                          WHERE    `guid` = "' . $params['guid'] . '"
                          AND      `uuid` = "' . $params['uuid'] . '"
                          ORDER BY `key` DESC
                          LIMIT    1' ) -> fetch_assoc( );

  // GUID

  if( ! $R )  $R = $mysqli -> query( 'SELECT   `key`,
                                               `key_user`
                                      FROM     `jc_user_device`
                                      WHERE    `guid` = "' . $params['guid'] . '"
                                      ORDER BY `key` DESC
                                      LIMIT    1' ) -> fetch_assoc( );

  // UUID

  if( ! $R )  $R = $mysqli -> query( 'SELECT   `key`,
                                               `key_user`
                                      FROM     `jc_user_device`
                                      WHERE    `uuid` = "' . $params['uuid'] . '"
                                      AND      `uuid`!= "undefined"
                                      ORDER BY `key` DESC
                                      LIMIT    1' ) -> fetch_assoc( );

  if( ! $R ) { mysqli_close( $mysqli ); return array( 'failure' => 'No GUID or UUID.',
                                                      'guid'    => $params['guid']   ,
                                                      'uuid'    => $params['uuid']    ); }



  // User Device

  $mysqli -> query( 'UPDATE `jc_user_device`
                     SET    `count`  = `count` + 1,
                            `update` = NOW( )
                     WHERE  `key`    = "' . $R['key'] . '"' );



  // User

  $R = $mysqli -> query( 'SELECT   *
                          FROM     `jc_user`
                          WHERE    `key` = "' . $R['key_user'] . '"' ) -> fetch_assoc( );

  if( ! $R ) { mysqli_close( $mysqli ); return array( 'failure' => 'No user.' ); }



  mysqli_close( $mysqli );

  return $R;

?>
