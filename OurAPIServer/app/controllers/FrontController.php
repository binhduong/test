<?php

namespace controllers;

/**
 * Class FrontController
 * @package controllers
 */
class FrontController
{

    /**
     * @param \Base $f3
     */
    function index(\Base $f3)
    {

        if (($f3->get('SHOW_LAST_POST') or $f3->get('SHOW_LAST_COMMENTS')) and !$this->is_connected()) {
            $f3->set('SHOW_LAST_POST', false);
            $f3->set('SHOW_LAST_COMMENTS', false);
        }

        if ($f3->get('SHOW_LAST_POST')) {
            if ($f3->devoid('LAST_POSTS')) {
                $LAST_POSTS = \Web::instance()->rss($f3->get('WWW_NEWS') . '/feed/', 10, 'title,link,pubDate');
                $f3->set('LAST_POSTS', isset($LAST_POSTS['feed']) ? $LAST_POSTS['feed'] : array(), 3600);
            } else
                $f3->set('LAST_POSTS', $f3->get('LAST_POSTS'));
        }

        if ($f3->get('SHOW_LAST_COMMENTS')) {
            if ($f3->devoid('LAST_COMMENTS')) {
                $LAST_COMMENTS = \Web::instance()->rss($f3->get('WWW_NEWS') . '/comments/feed/', 10, 'title,link,pubDate');
                $f3->set('LAST_COMMENTS', isset($LAST_COMMENTS['feed']) ? $LAST_COMMENTS['feed'] : array(), 3600);
            } else
                $f3->set('LAST_COMMENTS', $f3->get('LAST_COMMENTS'));
        }

        // Pagination
        $limit = 20;
        $count = $f3->get('s_api')->mapper->count();
        $pages = new \Pagination($count, $limit);
        $f3->set('pagebrowser', $pages->serve());
        $offset = $pages->getItemOffset();
        // End Pagination

        $l_api = $f3->get('s_api')->mapper->find(NULL, array('order' => 'last_change DESC', 'limit' => $limit, 'offset' => $offset));
        $f3->set('l_api', $l_api);

        $f3->set('content', 'page_home.html');
        echo \Template::instance()->render('__layout.html');

    }

    /**
     * @param \Base $f3
     */
    function redirect(\Base $f3)
    {
        $f3->reroute('/front');
    }

    /**
     * @param $f3
     */
    function beforeRoute(\Base $f3)
    {

        // 이 부분은 기본 도메인을 입력하여 접속한 사용자가 관리 페이지로 리다이렉션 되지 않도록 변경하였다.

        //if (!$f3->get('SESSION.is_logged'))
        //    $f3->reroute('/front/login?error=no have permissions.');

        if ( ! $f3 -> get( 'SESSION.is_logged' ) ) exit( '/f****/l****' ); // Jay Casee

        $f3->set('section_active', 'edit');
    }

    /**
     * Return True if the project is connected to internet
     * @return bool
     */
    private function is_connected()
    {

        $f3 = \Base::instance();

        $doc = $f3->get('WWW_DOC');
        $url = parse_url($doc);

        $connected = @fsockopen($url['host'], 80, $errno, $errstr, 5);
        if ($connected) {
            $is_conn = true;
            fclose($connected);
        } else
            $is_conn = false;
        return $is_conn;

    }

}