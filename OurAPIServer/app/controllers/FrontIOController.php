<?php

    namespace controllers;

    /**
     * Class FrontIOController
     * @package controllers
     */
    class FrontIOController
    {

        /**
         * Export API
         * @param \Base $f3
         * @param $params
         */
        function export(\Base $f3, $params)
        {

            $f3->set('id', $params['id']);

            $mapper = $f3->get('s_api')->mapper->load(array('id=?', $params['id']));
            $export = serialize($mapper->object());

            // Slugify name :-)
            require_once('3rdparty/slugify/src/Cocur/Slugify/Slugify.php');
            $slugify = new \Cocur\Slugify\Slugify();
            $name    = $slugify->slugify($mapper->name);

            header('Content-Disposition: attachment; ' . 'filename=' . $name . '.oas');
            header('Accept-Ranges: bytes');
            header('X-Powered-By: OurApiServer');

            echo $this->encrypt($export, 'oas2');

        }

        /**
         * Import API
         * @param \Base $f3
         */
        function import(\Base $f3)
        {

            \Web::instance()->receive('\controllers\FrontIOController->handleUpload', true);

        }

        /**
         * Handle the upload proccess
         * @param $args
         */
        function handleUpload($args)
        {

            $f3 = \Base::instance();

            $api = file_get_contents($args['tmp_name']);
            $api = $this->decrypt($api, 'oas2');
            $api = unserialize($api);

            $api_mapper = $f3->get('s_api')->mapper;

            // i basic
            $api_mapper->name        = $api->name;
            $api_mapper->description = $api->description;
            $api_mapper->path        = $api->path;
            $api_mapper->cache       = $api->cache;
            $api_mapper->throttling  = $api->throttling;
            $api_mapper->method      = $api->method;
            $api_mapper->output      = $api->output;
            // ii source
            $api_mapper->action = serialize(array(
                'type'   => $api->action['type'],
                'params' => isset($api->action['params']) ? $api->action['params'] : array(),
                'action' => $api->action['action'],
                'after'  => $api->action['after']
            ));
            // iii security
            $api_mapper->security = serialize(array(
                'type'       => $api->security['type'],
                'params'     => isset($api->security['params']) ? $api->security['params'] : array(),
                'encryption' => $api->security['encryption']
            ));
            // extra
            $api_mapper->last_change = date("Y-m-d H:i:s");

            $api_mapper->insert();

            $f3->reroute('/front');

        }

        /**
         * Encrypt API
         * @param $str
         * @param $key
         * @return string
         */
        private function encrypt($str, $key)
        {
            $block = mcrypt_get_block_size('des', 'ecb');
            $pad   = $block - (strlen($str) % $block);
            $str .= str_repeat(chr($pad), $pad);

            return mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
        }

        /**
         * Decrypt API
         * @param $str
         * @param $key
         * @return string
         */
        private function decrypt($str, $key)
        {
            $str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);

            $block = mcrypt_get_block_size('des', 'ecb');
            $pad   = ord($str[($len = strlen($str)) - 1]);

            return substr($str, 0, strlen($str) - $pad);
        }


        /**
         * @param $f3
         */
        function beforeRoute(\Base $f3)
        {
            if (!$f3->get('SESSION.is_logged'))
                $f3->reroute('/front/login?error=no have permissions.');

            $path = $f3->get('PATH');
            if ($f3->get('READONLY'))
                if ($path == '/front/import')
                    $f3->reroute('/front');

        }

    }