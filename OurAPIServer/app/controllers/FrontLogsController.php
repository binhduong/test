<?php

    namespace controllers;

    use models\LogsModel;

    /**
     * Class FrontLogsController
     * @package controllers
     */
    class FrontLogsController
    {

        /**
         * @param \Base $f3
         * @param $params
         */
        function index(\Base $f3, $params)
        {

            $get = $f3->get('GET');
            $f3->scrub($get);

            $f3->set('id', $params['id']);

            $logs = new LogsModel();
            $logs->connect($params['id']);

            $filter = null;

            if (isset($get['filter_path']) and $get['filter_path'])
                $filter = array('url LIKE ?', '%' . $get['filter_path'] . '%');

            if (isset($get['date_from']) and $get['date_from']) {
                if ($filter) {
                    $filter[0] = $filter[0] . ' and datetime >= ?';
                    $filter    = array_merge($filter, array($get['date_from'] . ' 00:00:00'));
                } else
                    $filter = array('datetime >= ?', $get['date_from'] . ' 00:00:00');
            }

            if (isset($get['date_to']) and $get['date_to']) {
                if ($filter) {
                    $filter[0] = $filter[0] . ' and datetime <= ?';
                    $filter    = array_merge($filter, array($get['date_to'] . ' 23:59:59'));
                } else
                    $filter = array('datetime  >= ?', $get['date_to'] . ' 23:59:59');
            }

            // Pagination
            $limit = 50;
            $count = $logs->mapper->count($filter);
            $pages = new \Pagination($count, $limit);
            $f3->set('pagebrowser', $pages->serve());
            $offset = $pages->getItemOffset();
            // End Pagination

            $f3->set('l_logs', $logs->mapper->find($filter, array('order' => 'datetime DESC', 'limit' => $limit, 'offset' => $offset)));

            $f3->set('content', 'page_logs.html');
            echo \Template::instance()->render('__layout.html');

        }

        /**
         * @param $f3
         */
        function beforeRoute(\Base $f3)
        {
            if (!$f3->get('SESSION.is_logged'))
                $f3->reroute('/front/login?error=no have permissions.');

            $f3->set('section_active', 'logs');
        }

    }