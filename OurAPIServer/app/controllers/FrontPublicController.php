<?php

    namespace controllers;


    class FrontPublicController
    {
        /**
         * @param \Base $f3
         * @param $params
         */
        function index(\Base $f3, $params)
        {
            $l_api = $f3->get('s_api')->mapper->find();
            $f3->set('l_api', $l_api);

            echo \Template::instance()->render('page_public.html');

        }

        /**
         * @param $f3
         */
        function beforeRoute(\Base $f3)
        {
            if (!$f3->get('SHOW_PUBLIC_ZONE'))
                $f3->reroute('/front');

            $f3->set('section_active', 'edit');
        }

    }