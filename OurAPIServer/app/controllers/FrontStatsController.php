<?php

    namespace controllers;

    use models\LogsModel;

    /**
     * Class FrontStatsController
     * @package controllers
     */
    class FrontStatsController
    {

        /**
         * @param \Base $f3
         * @param $params
         */
        function index(\Base $f3, $params)
        {

            $f3->set('id', $params['id']);

            // get 7 days ago date
            $date = new \DateTime();
            $date->sub(new \DateInterval('P7D'));

            // save the current and 7 days ago date
            if (!$f3->exists('GET.date_from'))
                $f3->set('GET.date_from', $date->format('Y-m-d'));
            if (!$f3->exists('GET.date_to'))
                $f3->set('GET.date_to', date('Y-m-d'));

            $f3->set('content', 'page_stats.html');
            echo \Template::instance()->render('__layout.html');

        }

        /**
         * @param \Base $f3
         * @param $params
         */
        function ajax_group_by_hits(\Base $f3, $params)
        {

            $get = $f3->get('GET');
            $f3->scrub($get);

            $api_id = $params['id'];

            $data = array();
            $logs = new LogsModel();

            $filter = '';
            if (isset($get['filter_path']) and $get['filter_path'])
                $filter = 'url LIKE "%' . $get['filter_path'] . '%"';

            if (isset($get['date_from']) and $get['date_from'])
                if ($filter)
                    $filter = $filter . ' and datetime >= "' . $get['date_from'] . ' 00:00:00"';
                else
                    $filter = 'datetime >= "' . $get['date_from'] . ' 00:00:00"';

            if (isset($get['date_to']) and $get['date_to'])
                if ($filter)
                    $filter = $filter . ' and datetime <= "' . $get['date_to'] . ' 23:59:59"';
                else
                    $filter = 'datetime <= "' . $get['date_to'] . ' 23:59:59"';

            if (!$filter)
                $filter = 1;

            $select = 'select count(id) as hits, datetime from oas_log_' . $api_id;
            $where  = 'where ' . $filter;
            $group  = 'group by datetime';
            $order  = 'order by datetime';

            $result = $logs->db->exec($select . ' ' . $where . ' ' . $group . ' ' . $order);

            foreach ($result as $i)
                $data[] = array(strtotime($i['datetime'] . " UTC") * 1000, round($i['hits'], 2));

            echo json_encode(array(array(
                'label'  => 'Hits',
                'data'   => $data,
                'bars'   => array('show' => true, 'fill' => true),
                'points' => array('show' => true)
            )), true);

        }

        /**
         * @param $f3
         */
        function beforeRoute(\Base $f3)
        {
            if (!$f3->get('SESSION.is_logged'))
                $f3->reroute('/front/login?error=no have permissions.');

            $f3->set('section_active', 'stats');
        }

    }