<?php

    namespace controllers;

    require_once '3rdparty/Config/Lite.php';

    use models\ApiModel;

    class FrontUpdateController
    {
        /**
         * @param \Base $f3
         * @param $params
         */
        function update(\Base $f3, $params)
        {

            $api = new ApiModel();
            $api->update();

            \Util::rrmdir('data/cache');
            \Util::rrmdir('data/temp');
            \Util::rrmdir('data/uploads');

            $config = new \Config_Lite('data/config.ini', LOCK_EX);
            $config->set('globals', 'WWW_DOC', "http://rabartu.info/documentation/ourapiserver")
                ->set('globals', 'WWW_NEWS', "http://rabartu.info/category/products/ourapiserver")
                ->set('globals', 'SHOW_LAST_COMMENTS', 0)
                ->set('globals', 'SHOW_PUBLIC_ZONE', 1)
                ->set('globals', 'STORE_LOGS', 1)
                ->set('globals', 'READONLY', 0)
                ->set('globals', 'DEPLOY', 0);
            $config->save();

            $f3->set('content', 'page_update.html');
            echo \Template::instance()->render('__layout.html');


        }

    }