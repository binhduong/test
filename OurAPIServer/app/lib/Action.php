<?php


class Action
{

    public static function execute($params)
    {
        $f3 = \Base::instance();

        if (Action::exist($query = $f3->get('api')->action['type']))
            return call_user_func_array('\\action\\' . $query . '::run', array($params));

        return FALSE;

    }

    public static function exist($action)
    {
        return file_exists('plugins/action/' . $action . '.php');
    }

    public static function list_all()
    {
        $result = array();
        $sec = array_diff(scandir('plugins/action/'), array('..', '.'));
        foreach ($sec as $l)
            $result[] = array(
                'name' => basename($l, ".php"),
                'mode' => Action::mode($l),
                'fields' => Action::fields($l),
                'help' => Action::help($l),
            );

        return $result;
    }

    public static function mode($l)
    {
        if (method_exists('\\action\\' . basename($l, ".php"), 'mode'))
            return call_user_func_array('\\action\\' . basename($l, ".php") . '::mode', array());
        else
            return 'csrc';

    }

    public static function fields($l)
    {

        return call_user_func_array('\\action\\' . basename($l, ".php") . '::fields', array());
    }

    public static function help($l)
    {

        return call_user_func_array('\\action\\' . basename($l, ".php") . '::help', array());
    }

} 