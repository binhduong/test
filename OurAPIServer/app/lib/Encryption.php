<?php


class Encryption
{

    public static function encrypt($value, $encrypt = null, $aditional = array())
    {
        if (!$encrypt) return $value;

        if (Encryption::exist($encrypt))
            return call_user_func_array('\\encryption\\' . $encrypt . '::encrypt', array($value, $aditional));

        return $value;

    }

    public static function exist($f)
    {
        return file_exists('plugins/encryption/' . $f . '.php');
    }

    public static function list_all($arg = NULL)
    {
        $result = array();
        $sec = array_diff(scandir('plugins/encryption/'), array('..', '.'));
        foreach ($sec as $l)
            $result[] = array(
                'name' => basename($l, ".php"),
            );

        return $result;
    }

}