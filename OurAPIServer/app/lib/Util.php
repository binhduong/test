<?php

/**
 * Class Util
 */
class Util
{

    /**
     * @param $string
     * @param $params
     * @return mixed
     */
    static function translate($string, $params)
    {
        return preg_replace_callback('/\@\w+/i',
            function ($matches) use ($params) {
                if (isset($params[substr($matches[0], 1)]))
                    return $params[substr($matches[0], 1)];
                return $matches[0];
            },
            $string);
    }


    /**
     * remove directories recursively
     * @param $dir
     */
    static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

} 