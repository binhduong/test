<?php

namespace models;

use DB\SQL;
use DB\SQL\Mapper;
use DB\SQL\Schema;

/**
 * Class LogsModel
 * @package models
 */
class LogsModel
{

    /**
     * @var \DB\SQL
     */
    public $db;
    /**
     * @var
     */
    public $mapper;

    /**
     *
     */
    function __construct()
    {

        $f3 = \Base::instance();

        $this->db = new SQL(
            "mysql:host=" . $f3->get('MYSQL_HOST') .
            ";port=" . $f3->get('MYSQL_PORT') .
            ";dbname=" . $f3->get('MYSQL_DB'),
            $f3->get('MYSQL_USER'),
            $f3->get('MYSQL_PASS'));

    }

    /**
     * @param $api_id
     */
    function connect($api_id)
    {
        $this->mapper = new LogsSQLModel($this->db, 'oas_log_' . $api_id);
    }

    /**
     * @param $api
     */
    function create_log_table($api)
    {

        $schema = new Schema($this->db);

        $table = $schema->createTable('oas_log_' . $api->id);

        $table->addColumn('host')->type($schema::DT_VARCHAR256);
        $table->addColumn('url')->type($schema::DT_VARCHAR512);
        $table->addColumn('params')->type($schema::DT_TEXT);
        $table->addColumn('agent')->type($schema::DT_VARCHAR512);
        $table->addColumn('datetime')->type($schema::DT_DATETIME);

        $table->build();

    }

    /**
     * @param $api
     */
    function delete_log_table($api)
    {


        $schema = new Schema($this->db);

        $table = $schema->dropTable('oas_log_' . $api->id);

    }


}

/**
 * Class LogsSQLModel
 * @package models
 */
class LogsSQLModel extends Mapper
{


}
