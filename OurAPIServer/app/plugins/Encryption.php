<?php

namespace plugins;


/**
 * Interface Encryption
 * @package plugins
 */
interface Encryption
{
    /**
     * @param $value
     * @param $aditional
     * @return mixed
     */
    public static function encrypt($value, $aditional);

} 