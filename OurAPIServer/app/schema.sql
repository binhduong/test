-- OurApiServer SQL Dump
-- version 2.0+
-- http://rabartu.info/portfolio-item/ourapiserver/

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `oas_api`
--

DROP TABLE IF EXISTS `oas_api`;
CREATE TABLE IF NOT EXISTS `oas_api` (
`id` int(4) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `path` varchar(50) COLLATE utf8_bin NOT NULL,
  `cache` int(11) NOT NULL,
  `throttling` int(11) NOT NULL,
  `method` varchar(50) COLLATE utf8_bin NOT NULL,
  `output` varchar(50) COLLATE utf8_bin NOT NULL,
  `action` text COLLATE utf8_bin NOT NULL,
  `security` text COLLATE utf8_bin NOT NULL,
  `last_change` datetime NOT NULL,
  `is_public` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `oas_api`
--

INSERT INTO `oas_api` (`id`, `name`, `description`, `path`, `cache`, `throttling`, `method`, `output`, `action`, `security`, `last_change`, `is_public`) VALUES
(1, 'Hello', '<p><span style="line-height:1.6">The fast way tho say &quot;Hello&quot;.</span></p>', '/hello', 0, 0, 'all', 'json', 'a:4:{s:4:"type";s:7:"printer";s:6:"params";a:0:{}s:6:"action";s:5:"Hello";s:5:"after";s:15:"return $result;";}', 'a:3:{s:4:"type";s:4:"none";s:6:"params";a:0:{}s:10:"encryption";s:6:"drupal";}', '2014-10-23 22:42:58', 0),
(2, 'Calculator', '<p>@op =&gt; operation [sum|diff|mult|div]</p><p>@a =&gt; param1</p><p>@b =&gt; param2</p><p>Execute simple math @operations.</p><p><em>Example of EVALUATE action</em></p>', '/@op/@a/@b', 60, 0, 'all', 'json', 'a:4:{s:4:"type";s:8:"evaluate";s:6:"params";a:0:{}s:6:"action";s:252:"switch ("@op") {\r\n	case "sum":\r\n		return @a+@b;\r\n		break;\r\n	case "diff":\r\n		return @a-@b;\r\n		break;\r\n	case "mult":\r\n		return @a*@b;\r\n		break;\r\n	case "div":\r\n		return @a/@b;\r\n		break;\r\n	default:\r\n		return "operations sum|diff|mult|div";\r\n		break;\r\n}\r\n\r\n";s:5:"after";s:29:"return $result;\r\n\r\n\r\n\r\n\r\n\r\n\r\n";}', 'a:3:{s:4:"type";s:4:"none";s:6:"params";a:0:{}s:10:"encryption";s:5:"plain";}', '2014-10-23 22:43:04', 0),
(3, 'Free Space', '<p>Know the &quot;free space&quot; of @drive on Windows OS.</p><p><em>Example of EXECUTE action with &quot;after&quot; action.</em></p>', '/free/@drive', 0, 0, 'all', 'json', 'a:4:{s:4:"type";s:7:"execute";s:6:"params";a:0:{}s:6:"action";s:30:"fsutil volume diskfree @drive:";s:5:"after";s:126:"$result = explode("\\n",$result[0]); \r\n\r\n$result = array_filter($result,function($i){ return (trim($i)); });\r\n\r\nreturn $result;";}', 'a:3:{s:4:"type";s:4:"none";s:6:"params";a:0:{}s:10:"encryption";s:6:"drupal";}', '2014-10-23 22:43:13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oas_log_1`
--

DROP TABLE IF EXISTS `oas_log_1`;
CREATE TABLE IF NOT EXISTS `oas_log_1` (
`id` int(11) NOT NULL,
  `host` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `agent` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oas_log_2`
--

DROP TABLE IF EXISTS `oas_log_2`;
CREATE TABLE IF NOT EXISTS `oas_log_2` (
`id` int(11) NOT NULL,
  `host` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `agent` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oas_log_3`
--

DROP TABLE IF EXISTS `oas_log_3`;
CREATE TABLE IF NOT EXISTS `oas_log_3` (
`id` int(11) NOT NULL,
  `host` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `agent` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oas_test`
--

DROP TABLE IF EXISTS `oas_test`;
CREATE TABLE IF NOT EXISTS `oas_test` (
`id` int(11) NOT NULL,
  `user` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oas_api`
--
ALTER TABLE `oas_api`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oas_log_1`
--
ALTER TABLE `oas_log_1`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oas_log_2`
--
ALTER TABLE `oas_log_2`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oas_log_3`
--
ALTER TABLE `oas_log_3`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oas_test`
--
ALTER TABLE `oas_test`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oas_api`
--
ALTER TABLE `oas_api`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oas_log_1`
--
ALTER TABLE `oas_log_1`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oas_log_2`
--
ALTER TABLE `oas_log_2`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oas_log_3`
--
ALTER TABLE `oas_log_3`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oas_test`
--
ALTER TABLE `oas_test`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;