function in_loading_state() {
    $('.container,.edit-container,.stats-container').css('opacity', 0.2);

    $('#loading').css('display', 'block');
}

function out_loading_state() {
    setTimeout(function () {
        $('.container,.edit-container,.stats-container').css('opacity', 1);

        $('#loading').css('display', 'none');
    }, 500);
}