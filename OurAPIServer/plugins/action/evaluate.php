<?php

namespace action;

use plugins\Action;

class evaluate implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $action = trim($f3->get('api')->action['action']);
        $action = \Util::translate($action, $params);

        return eval($action);
    }

    public static function mode()
    {
        return 'php';
    }

    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return '
                In the Action Field put a PHP Sentence for Evaluate.<br>
                At the end is required the sentence <code>return</code><br>
                Example: <code>$a = 5 ; $a = $a + 23 ; return $a;</code><br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                The <code>eval()</code> language construct is <strong>very dangerous</strong> because it allows execution of arbitrary PHP code.<br>
                ';
    }

}
