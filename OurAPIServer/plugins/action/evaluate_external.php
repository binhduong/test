<?php

namespace action;

use plugins\Action;

class evaluate_external implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $action = trim($f3->get('api')->action['action']);
        $action = \Util::translate($action, $params);

        return require($action);
    }

    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return '
                This Source is designed for <b>PHP</b> programmers.<br>
                In the field action we only need insert the path of the main file of your external code.<br>
                <em>Ex : </em> if we create a script on <b>data/hello.php</b><br>
                with the code: <br> <code>< ? php<br><spam style="margin-left: 20px">return "hello";</spam></code><br>
                Then for execute the file and see "hello" on the browser,<br>
                we need insert on "Action" the value <code>data/hello.php</code><br>
                It\'s IMPORTANT the sentence <code>return</code> at the end of the "EXTERNAL" file.
                ';
    }

}
