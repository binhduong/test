<?php

namespace action;

use plugins\Action;

class odbc implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $dsn = $f3->get('api')->action['params']['dsn'];
        $user = $f3->get('api')->action['params']['user'];
        $pass = $f3->get('api')->action['params']['pass'];

        $db_query = $f3->get('api')->action['action'];
        $db_query = \Util::translate($db_query, $params);

        $db = new \DB\SQL("odbc:" . $dsn, $user, $pass);

        return $db->exec($db_query);
    }

    public static function mode()
    {
        return 'sql';
    }

    public static function fields()
    {
        return array(
            'dsn' => array('type' => 'text', 'title' => 'DSN', 'help' => 'ODBC DSN'),
            'user' => array('type' => 'text', 'title' => 'User', 'help' => 'User'),
            'pass' => array('type' => 'password', 'title' => 'Password', 'help' => 'Password'),
        );
    }

    public static function help()
    {
        return '
                In the Action Field put a SQL Query.<br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                ';
    }

}
