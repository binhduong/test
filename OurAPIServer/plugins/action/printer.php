<?php

namespace action;

use plugins\Action;

class printer implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $action = trim($f3->get('api')->action['action']);
        $action = \Util::translate($action, $params);

        return array($action);
    }

    public static function mode()
    {
        return 'php';
    }


    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return '
                In the Action Field put a String to Show in the API Request.<br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                for example if in the url you have <code>/hello/@user</code> you can access to <code>@user</code>.
               ';
    }
}
