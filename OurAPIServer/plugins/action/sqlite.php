<?php

namespace action;

use plugins\Action;

class sqlite implements Action
{

    public static function mode()
    {
        return 'mysql';
    }

    public static function run($params)
    {

        $f3 = \Base::instance();

        $db_name = $f3->get('api')->action['params']['db_name'];
        $db_query = $f3->get('api')->action['action'];

        $db_query = \Util::translate($db_query, $params);

        $db = new \DB\SQL('sqlite:' . $db_name);

        return $db->exec($db_query);

    }

    public static function fields()
    {
        return array(
            'db_name' => array('type' => 'text', 'title' => 'Database Path', 'help' => 'Path to SQLITE File'),
        );
    }

    public static function help()
    {
        return '
                In the Action Field put a SQL Query.<br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                ';
    }

}
