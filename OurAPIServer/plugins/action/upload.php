<?php

namespace action;

use plugins\Action;

class upload implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        \Web::instance()->receive('\action\upload::handleUpload', TRUE);

        return $f3->get('upload_message');

    }

    public static function handleUpload($args)
    {

        $f3 = \Base::instance();

        $destination = $f3->get('api')->action['action'];

        if (copy($args['tmp_name'], $destination . basename($args['name'])))
            $f3->set('upload_message', 'File uploaded in ' . $destination . $args['name']);
        else
            $f3->set('upload_message', 'ERROR');

    }

    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return '
                In the Action Field put the Destination for the Uploaded File.<br>
                Need a Absolute path end on <code>/</code> for example :
                <code>C:/Program Files/Ampps/www/api2/data/uploads/</code>
                ';

    }

}
