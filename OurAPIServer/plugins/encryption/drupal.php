<?php

namespace encryption;

use plugins\Encryption;

class drupal implements Encryption
{

    public static function encrypt($password, $aditional = array())
    {

        require_once '3rdparty/drupal/includes/password.inc';

        if (substr($aditional['stored'], 0, 2) == 'U$') {
            // This may be an updated password from user_update_7000(). Such hashes
            // have 'U' added as the first character and need an extra md5().
            $stored_hash = substr($aditional['stored'], 1);
            $password = md5($password);
        } else {
            $stored_hash = $aditional['stored'];
        }

        $type = substr($stored_hash, 0, 3);

        switch ($type) {
            case '$S$':
                // A normal Drupal 7 password using sha512.
                $hash = _password_crypt('sha512', $password, $stored_hash);
                break;
            case '$H$':
                // phpBB3 uses "$H$" for the same thing as "$P$".
            case '$P$':
                // A phpass password generated using md5.  This is an
                // imported password or from an earlier Drupal version.
                $hash = _password_crypt('md5', $password, $stored_hash);
                break;
            default:
                return $password;
        }

        return $hash;

    }

}
