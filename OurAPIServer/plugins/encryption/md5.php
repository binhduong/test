<?php

namespace encryption;

use plugins\Encryption;

class md5 implements Encryption
{
    public static function encrypt($value, $aditional = array())
    {
        return md5($value);
    }

}
