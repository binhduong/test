<?php

namespace encryption;

use plugins\Encryption;

class sha1 implements Encryption
{
    public static function encrypt($value, $aditional = array())
    {
        return sha1($value);
    }

}
