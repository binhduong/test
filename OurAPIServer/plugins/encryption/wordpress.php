<?php

namespace encryption;

use plugins\Encryption;

class wordpress implements Encryption
{
    public static function encrypt($value, $aditional = array())
    {
        require_once '3rdparty/wordpress/wp-includes/class-phpass.php';

        $wp_hasher = new \PasswordHash(8, true);

        return $wp_hasher->crypt_private($value, $aditional['stored']);
    }

}
