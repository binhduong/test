<?php


    namespace output;


    use plugins\Output;

    class json implements Output
    {
        public static function send($data)
        {

            header('Content-type: application/json');

            $output = new \Luracast\Restler\Format\JsonFormat;
            echo $output->encode($data, true);
        }
    }