<?php

    // This file has been added by Jay Casee.

    namespace output;


    use plugins\Output;

    class jsonp implements Output
    {
        public static function send($data)
        {

            header('Content-type: application/json');

            $output = new \Luracast\Restler\Format\JsFormat;
            echo $output->encode($data, true);
        }
    }