<?php

    namespace output;

    use plugins\Output;

    class serialize implements Output
    {
        public static function send($data)
        {
            header('Content-type: text/plain');

            echo serialize($data);
        }

    }
