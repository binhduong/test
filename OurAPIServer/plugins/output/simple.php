<?php

    namespace output;

    use plugins\Output;

    class simple implements Output
    {
        public static function send($data)
        {
            header('Content-type: text/plain');

            if (is_array($data))
                echo implode($data);
            else
                echo $data;
        }

    }
