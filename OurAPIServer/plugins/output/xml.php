<?php

    namespace output;

    use plugins\Output;

    class xml implements Output
    {
        public static function send($data)
        {
            header('Content-type: application/xml');

            $output = new \Luracast\Restler\Format\XmlFormat;
            echo $output->encode($data, true);
        }

    }
