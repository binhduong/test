<?php

    namespace output;

    use plugins\Output;

    class yaml implements Output
    {
        public static function send($data)
        {

            header('Content-type: application/yaml');

            $output = new \Luracast\Restler\Format\YamlFormat;
            echo $output->encode($data, true);
        }

    }
