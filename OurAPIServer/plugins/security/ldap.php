<?php

namespace security;

use plugins\Security;

class ldap implements Security
{
    public static function check($params)
    {
        $f3 = \Base::instance();


        $url = $f3->get('api')->action['params']['url'];
        $port = $f3->get('api')->action['params']['port'] ? $f3->get('api')->action['params']['port'] : 389;
        $base = $f3->get('api')->action['params']['base'];
        $user = $f3->get('api')->action['params']['user'];
        $pass = $f3->get('api')->action['params']['pass'];

        $user_name = $f3->get('api')->security['params']['user_name'];
        $pass_name = $f3->get('api')->security['params']['pass_name'];

        $user_value = $params[$user_name];
        $pass_value = $params[$pass_name];

        $ldap = @ldap_connect($url, $port);
        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        if ($user)
            ldap_bind($ldap, $user, $pass);

        $result = ldap_search($ldap, $base, $user_value);
        if (ldap_count_entries($ldap, $result)) {
            $info = ldap_get_entries($ldap, $result);
            if ($info) {
                if (@ldap_bind($ldap, $info[0]['dn'], $pass_value)) {
                    return true;
                }
            }
        }

        return false;

    }

    public static function fields()
    {
        return array(
            'url' => array('type' => 'text', 'title' => 'URL', 'help' => 'URL'),
            'port' => array('type' => 'text', 'title' => 'Port', 'help' => 'Server Port (leave in blank to use "389")'),
            'base' => array('type' => 'text', 'title' => 'Base DN', 'help' => 'DN of the Base for get the USER'),
            'user' => array('type' => 'text', 'title' => 'User', 'help' => 'User for connect with the LDAP Server'),
            'pass' => array('type' => 'password', 'title' => 'Password', 'help' => 'Password for connect with the LDAP Server'),
            'user_name' => array('type' => 'text', 'title' => 'User Parameter', 'help' => 'Name of User Variable (this variable must be a param).'),
            'pass_name' => array('type' => 'text', 'title' => 'Password Parameter', 'help' => 'Name of Password Variable (this variable must be a param).'),
        );
    }

    public static function help()
    {
        return '
                LDAP Security
                ';

    }

}
