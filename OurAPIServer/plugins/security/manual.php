<?php

namespace security;

use plugins\Security;

class manual implements Security
{
    public static function check($args)
    {
        $f3 = \Base::instance();

        $user_name = $f3->get('api')->security['params']['user_name'];
        $user_value = $f3->get('api')->security['params']['user_value'];
        $pass_name = $f3->get('api')->security['params']['pass_name'];
        $pass_value = $f3->get('api')->security['params']['pass_value'];
        $encryption = $f3->get('api')->security['encryption'];

        if (isset($args[$user_name]) and
            isset($args[$pass_name]) and
            $user_value == $args[$user_name] and
            $pass_value == \Encryption::encrypt($args[$pass_name], $encryption, array('stored' => $pass_value))
        )
            return true;

        return false;

    }

    public static function fields()
    {
        return array(
            'user_name' => array('type' => 'text', 'title' => 'User Param.', 'help' => 'Name of User Variable (this variable must be a param).'),
            'user_value' => array('type' => 'text', 'title' => 'User', 'help' => 'User Value'),
            'pass_name' => array('type' => 'text', 'title' => 'Password Param.', 'help' => 'Name of Password Variable (this variable must be a param).'),
            'pass_value' => array('type' => 'text', 'title' => 'Password', 'help' => 'Password Value'),
        );
    }

    public static function help()
    {
        return '
                Manual security need define the name of @params User and Password for get in the URL.
                ';
    }

}
