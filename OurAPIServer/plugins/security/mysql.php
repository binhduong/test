<?php

namespace security;

use plugins\Security;

class mysql implements Security
{
    public static function check($params)
    {
        $f3 = \Base::instance();

        $db_host = $f3->get('api')->security['params']['db_host'];
        $db_port = $f3->get('api')->security['params']['db_port'] ? $f3->get('api')->security['params']['db_port'] : 3306;
        $db_name = $f3->get('api')->security['params']['db_name'];
        $db_user = $f3->get('api')->security['params']['db_user'];
        $db_pass = $f3->get('api')->security['params']['db_pass'];

        $user_name = $f3->get('api')->security['params']['user_name'];
        $user_query = $f3->get('api')->security['params']['user'];
        $pass_name = $f3->get('api')->security['params']['pass_name'];
        $pass_query = $f3->get('api')->security['params']['pass'];
        $encryption = $f3->get('api')->security['encryption'];

        $user_query = \Util::translate($user_query, $params);
        $pass_query = \Util::translate($pass_query, $params);

        $db = new \DB\SQL('mysql:host=' . $db_host . ';port=' . $db_port . ';dbname=' . $db_name, $db_user, $db_pass);

        $user_result = current(current($db->exec($user_query)));
        $pass_result = current(current($db->exec($pass_query)));

        if (isset($params[$user_name]) and
            isset($params[$pass_name]) and
            $user_result == $params[$user_name] and
            $pass_result == \Encryption::encrypt($params[$pass_name], $encryption, array('stored' => $pass_result))
        )
            return true;

        return false;

    }

    public static function fields()
    {
        return array(
            'db_host' => array('type' => 'text', 'title' => 'Database Host', 'help' => 'MySQL Server Host'),
            'db_port' => array('type' => 'text', 'title' => 'Database Port', 'help' => 'MySQL Server Port (leave in blank to use "3306")'),
            'db_name' => array('type' => 'text', 'title' => 'Database Name', 'help' => 'DataBase Name'),
            'db_user' => array('type' => 'text', 'title' => 'Database User', 'help' => 'DataBase User'),
            'db_pass' => array('type' => 'password', 'title' => 'Database Password', 'help' => 'DataBase Password'),
            'user_name' => array('type' => 'text', 'title' => 'User Parameter', 'help' => 'Name of User Variable (this variable must be a param).'),
            'user' => array('type' => 'textarea', 'title' => 'User Query', 'help' => 'Query for get the User'),
            'pass_name' => array('type' => 'text', 'title' => 'Password Parameter', 'help' => 'Name of Password Variable (this variable must be a param).'),
            'pass' => array('type' => 'textarea', 'title' => 'Password Query', 'help' => 'Query for get the Password')
        );
    }

    public static function help()
    {
        return '
                In the User and Password Fields put a SQL Querys.<br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                ';

    }
}
