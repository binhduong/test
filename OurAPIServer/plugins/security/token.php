<?php

namespace security;

use plugins\Security;

class token implements Security
{
    public static function check($args)
    {
        $f3 = \Base::instance();

        $token_name = $f3->get('api')->security['params']['token_name'];
        $password = $f3->get('api')->security['params']['token_value'];
        $encryption = $f3->get('api')->security['encryption'];

        if (isset($args[$token_name]) and
            $password == \Encryption::encrypt($args[$token_name], $encryption, array('stored' => $password))
        )
            return true;

        return false;

    }

    public static function fields()
    {
        return array(
            'token_name' => array('type' => 'text', 'title' => 'Parameter', 'help' => 'Name of Token Variable (this variable must be a param).'),
            'token_value' => array('type' => 'text', 'title' => 'Value', 'help' => 'Token Value'),
        );
    }

    public static function help()
    {
        return '
                Manual security need define the name of @parameter Token for get in the URL.
                ';
    }

}
